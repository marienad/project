package ex2;

/**
 * Класс лаунчер
 */
public class Main {

    public static void main(String[] args) {
        Provider provider = new Provider();
        Sender sender = new Sender();
        while (provider.hasMail()) {
            if (sender.isFree())
                sender.send(provider.getMail());
        }
    }
}

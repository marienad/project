package ex2;

/**
 * Класс описывающий обьект-письмо
 */
public class Mail {
    private String text;
    private String adressee;

    public Mail(String text, String adressee) {
        this.text = text;
        this.adressee = adressee;
    }

    public String getText() {
        return text;
    }

    public String getAdressee() {
        return adressee;
    }
}

package ex2;
/**
 * Класс описывающий объект-поставщик
 */

import java.sql.*;

public class Provider {
    private static Connection c;
    private static Statement statement;
    private static int amount;

    public Provider() {
        connect();
        try {
            statement = c.createStatement();
        } catch (SQLException e) {
            System.out.println(e.getMessage());
        }
        amount = 0;
    }

    private static void connect() {
        try {
            Class.forName("org.postgresql.Driver");
            c = DriverManager
                    .getConnection("jdbc:postgresql://localhost:5432/mail",
                            "postgres", "123");
        } catch (Exception e) {
            e.printStackTrace();
            System.err.println(e.getClass().getName() + ": " + e.getMessage());
            System.exit(0);
        }
        System.out.println("Opened database successfully");
    }

    public boolean hasMail() {
        try {
            return statement.executeQuery("SELECT * FROM mails").next();
        } catch (SQLException e) {
            System.out.println(e.getMessage());
        }
        return false;
    }

    public Mail getMail() {
        String text = "";
        String adressee = "";
        try {
            text = String.valueOf(statement.executeQuery("SELECT text FROM mails WHERE id=" + amount));
            adressee = String.valueOf(statement.executeQuery("SELECT adressee FROM mails WHERE id=" + amount));
        } catch (SQLException e) {
            System.out.println(e.getMessage());
        }
        return new Mail(text, adressee);
    }
}

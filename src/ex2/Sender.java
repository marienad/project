package ex2;

/**
 * Класс описывающий обьект-отправитель
 */
public class Sender {
    private long lastSendDate;

    public boolean isFree() {
        if (lastSendDate > System.currentTimeMillis() + 5000) {
            return true;
        }
        return false;
    }

    void send(Mail mail) {
        System.out.println("send \"" + mail.getText() + " \" to " + mail.getAdressee());
        lastSendDate = System.currentTimeMillis();
    }
}

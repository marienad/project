package ex3.elevators;

import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;

public class House {
    private static Scanner scanner = new Scanner(System.in);

    public static void main(String[] args) {
        List<Elevator> list = getElevators();
        boolean exit = false;
        while (!exit) {
            int floor = scanner.nextInt();
            if (floor <= 0 || floor > 9) {
                exit = true;
            } else {
                callElevator(list, floor);
            }
        }
    }

    private static List<Elevator> getElevators() {
        List<Elevator> list = new ArrayList<>();
        for (int i = 0; i < 3; i++) {
            list.add(new Elevator());
        }
        return list;
    }

    private static void callElevator(List<Elevator> list, int floor) {
        Elevator e = list.get(0);
        int a = 0;
        for (int i = 1; i < list.size(); i++) {
            if (Math.abs(floor - e.getFloor()) > Math.abs(floor - list.get(i).getFloor())) {
                a = i;
                e = list.get(i);
            }
        }
        list.get(a).setFloor(floor);
        try {
            list.get(a + 1).setFloor(1);
        } catch (IndexOutOfBoundsException e1) {
            list.get(a - 1).setFloor(1);
        }
        for (int i = 0; i < list.size(); i++) {
            System.out.println(list.get(i).getFloor());
        }
    }
}

package ex3.elevators;

public class Elevator {
    int floor;

    public Elevator() {
        this.floor = 1;
    }

    public int getFloor() {
        return floor;
    }

    public void setFloor(int floor) {
        this.floor = floor;
    }
}

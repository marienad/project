package ex1;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.List;

public class FindThread implements Runnable {

    private String pathToFile;
    private String text;

    public FindThread(String text, String pathToFile) {
        this.pathToFile = pathToFile;
        this.text = text;
    }

    public void run() {
        try {
            List<String> list = Files.readAllLines(Paths.get(pathToFile));
            for (int i = 0; i < list.size(); i++) {
                if (list.get(i).toLowerCase().contains(text.toLowerCase())) {
                    System.out.println(pathToFile);
                    return;
                }
            }
        } catch (IOException e) {
           System.out.println("ошибка чтения " + pathToFile);
        }

    }
}
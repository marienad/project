package ex1;

import java.io.File;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Scanner;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

public class Main {
    private static String text;
    private static ExecutorService pool;
    private static Scanner scanner = new Scanner(System.in);

    public static void main(String[] args) {
        System.out.println("Текст для поиска: ");
        text = scanner.next();
        pool = Executors.newFixedThreadPool(100);
        List<File> roots = Arrays.asList(File.listRoots());
        List<String> rootList = convert(roots);
        find(rootList, "");
        //test();
    }

    private static List<String> convert(List<File> roots) {
        List<String> list = new ArrayList<>();
        for (int i = 0; i < roots.size(); i++) {
            list.add(roots.get(i).getPath());
        }
        return list;
    }

    private static void find(List<String> list, String dirName) {
        for (int i = 0; i < list.size(); i++) {
            analize(dirName + File.separator + list.get(i));
        }
    }

    static void analize(String dirName) {
        File f = new File(dirName);
        if (f.isFile()) {
            pool.submit(new FindThread(text, dirName));
            return;
        }
        if (f.list() == null) {
            return;
        }
        List<String> dirList = Arrays.asList(f.list());
        find(dirList, dirName);
    }

    private static void test() {
        File file = new File("D:\\");
        List<String> list = Arrays.asList(file.list());
        find(list,"D:\\");
    }
}
